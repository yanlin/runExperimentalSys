void writeAllLists(){

  TString htag = "h021";
  TString ptag = "p3472";
  TString path = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/"+htag+"/";

  std::vector<TString> mcTypes = {"mc16a", "mc16d"};
  std::vector<TString> prodModes = {"ggH", "VBF", "WH", "ZH", "ggZH", "ttH", "bbH", "tHjb", "tWH"};
  std::vector<TString> sysTypes = {"PhotonSys", "FlavorSys", "LeptonMETSys"};

  std::map<TString, std::vector<TString>> sampleNames;
  sampleNames["ggH"] = {"PowhegPy8_NNLOPS_ggH125"};
  sampleNames["VBF"] = {"PowhegPy8_NNPDF30_VBFH125"};
  sampleNames["WH"] = {"PowhegPy8_WpH125J", "PowhegPy8_WmH125J"};
  sampleNames["ZH"] = {"PowhegPy8_ZH125J"};
  sampleNames["ggZH"] = {"PowhegPy8_ggZH125"};
  sampleNames["ttH"] = {"PowhegPy8_ttH125"};
  sampleNames["bbH"] = {"PowhegPy8_bbH125"};
  sampleNames["tHjb"] = {"MGPy8_tHjb125_yt_plus1"};
  sampleNames["tWH"] = {"aMCnloHwpp_tWH125_yt_plus1"};

  std::map<TString,int> nFiles;

  // PhotonSys
  nFiles["PhotonSys mc16a PowhegPy8_NNLOPS_ggH125"] = 15;
  nFiles["PhotonSys mc16a PowhegPy8_NNPDF30_VBFH125"] = 6;
  nFiles["PhotonSys mc16a PowhegPy8_WpH125J"] = 2;
  nFiles["PhotonSys mc16a PowhegPy8_WmH125J"] = 1;
  nFiles["PhotonSys mc16a PowhegPy8_ZH125J"] = 3;
  nFiles["PhotonSys mc16a PowhegPy8_ggZH125"] = 1;
  nFiles["PhotonSys mc16a PowhegPy8_ttH125"] = 8;
  nFiles["PhotonSys mc16a PowhegPy8_bbH125"] = 1;
  nFiles["PhotonSys mc16a MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["PhotonSys mc16a aMCnloHwpp_tWH125_yt_plus1"] = 1;

  nFiles["PhotonSys mc16d PowhegPy8_NNLOPS_ggH125"] = 15;
  nFiles["PhotonSys mc16d PowhegPy8_NNPDF30_VBFH125"] = 0;
  nFiles["PhotonSys mc16d PowhegPy8_WpH125J"] = 2;
  nFiles["PhotonSys mc16d PowhegPy8_WmH125J"] = 1;
  nFiles["PhotonSys mc16d PowhegPy8_ZH125J"] = 2;
  nFiles["PhotonSys mc16d PowhegPy8_ggZH125"] = 1;
  nFiles["PhotonSys mc16d PowhegPy8_ttH125"] = 10;
  nFiles["PhotonSys mc16d PowhegPy8_bbH125"] = 1;
  nFiles["PhotonSys mc16d MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["PhotonSys mc16d aMCnloHwpp_tWH125_yt_plus1"] = 1;


  // FlavorSys
  nFiles["FlavorSys mc16a PowhegPy8_NNLOPS_ggH125"] = 18;
  nFiles["FlavorSys mc16a PowhegPy8_NNPDF30_VBFH125"] = 10;
  nFiles["FlavorSys mc16a PowhegPy8_WpH125J"] = 2;
  nFiles["FlavorSys mc16a PowhegPy8_WmH125J"] = 1;
  nFiles["FlavorSys mc16a PowhegPy8_ZH125J"] = 3;
  nFiles["FlavorSys mc16a PowhegPy8_ggZH125"] = 1;
  nFiles["FlavorSys mc16a PowhegPy8_ttH125"] = 13;
  nFiles["FlavorSys mc16a PowhegPy8_bbH125"] = 1;
  nFiles["FlavorSys mc16a MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["FlavorSys mc16a aMCnloHwpp_tWH125_yt_plus1"] = 1;

  nFiles["FlavorSys mc16d PowhegPy8_NNLOPS_ggH125"] = 21;
  nFiles["FlavorSys mc16d PowhegPy8_NNPDF30_VBFH125"] = 6;
  nFiles["FlavorSys mc16d PowhegPy8_WpH125J"] = 1;
  nFiles["FlavorSys mc16d PowhegPy8_WmH125J"] = 1;
  nFiles["FlavorSys mc16d PowhegPy8_ZH125J"] = 3;
  nFiles["FlavorSys mc16d PowhegPy8_ggZH125"] = 1;
  nFiles["FlavorSys mc16d PowhegPy8_ttH125"] = 16;
  nFiles["FlavorSys mc16d PowhegPy8_bbH125"] = 1;
  nFiles["FlavorSys mc16d MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["FlavorSys mc16d aMCnloHwpp_tWH125_yt_plus1"] = 1;

  // LeptonMETSys
  nFiles["LeptonMETSys mc16a PowhegPy8_NNLOPS_ggH125"] = 24;
  nFiles["LeptonMETSys mc16a PowhegPy8_NNPDF30_VBFH125"] = 12;
  nFiles["LeptonMETSys mc16a PowhegPy8_WpH125J"] = 2;
  nFiles["LeptonMETSys mc16a PowhegPy8_WmH125J"] = 2;
  nFiles["LeptonMETSys mc16a PowhegPy8_ZH125J"] = 3;
  nFiles["LeptonMETSys mc16a PowhegPy8_ggZH125"] = 1;
  nFiles["LeptonMETSys mc16a PowhegPy8_ttH125"] = 21;
  nFiles["LeptonMETSys mc16a PowhegPy8_bbH125"] = 1;
  nFiles["LeptonMETSys mc16a MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["LeptonMETSys mc16a aMCnloHwpp_tWH125_yt_plus1"] = 1;

  nFiles["LeptonMETSys mc16d PowhegPy8_NNLOPS_ggH125"] = 26;
  nFiles["LeptonMETSys mc16d PowhegPy8_NNPDF30_VBFH125"] = 7;
  nFiles["LeptonMETSys mc16d PowhegPy8_WpH125J"] = 2;
  nFiles["LeptonMETSys mc16d PowhegPy8_WmH125J"] = 2;
  nFiles["LeptonMETSys mc16d PowhegPy8_ZH125J"] = 3;
  nFiles["LeptonMETSys mc16d PowhegPy8_ggZH125"] = 1;
  nFiles["LeptonMETSys mc16d PowhegPy8_ttH125"] = 18;
  nFiles["LeptonMETSys mc16d PowhegPy8_bbH125"] = 1;
  nFiles["LeptonMETSys mc16d MGPy8_tHjb125_yt_plus1"] = 1;
  nFiles["LeptonMETSys mc16d aMCnloHwpp_tWH125_yt_plus1"] = 1;

  for(TString sys : sysTypes){
    for(TString mc : mcTypes){
      for(TString mode : prodModes){
    
	std::ofstream file;
	file.open("listEOS_"+htag+"_"+mc+"_"+mode+"_"+sys+".txt");

	for(TString samp : sampleNames[mode]){
	  
	  if(nFiles[sys+" "+mc+" "+samp]==1) file << path+mc+"/"+sys+"/"+mc+"."+samp+".MxAOD"+sys+"."+ptag+"."+htag+".root" << std::endl;
	  else{
	    for(int i=1; i<=nFiles[sys+" "+mc+" "+samp]; i++) file << path+mc+"/"+sys+"/"+mc+"."+samp+".MxAOD"+sys+"."+ptag+"."+htag+".root/"+mc+"."+samp+".MxAOD"+sys+".p3472."+htag+".0"<<Form("%.2i",i)<<".root" << std::endl;
	  }

	}

	file.close();

      }
    }
  }


}
