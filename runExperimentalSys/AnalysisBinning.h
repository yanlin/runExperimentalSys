#ifndef runExperimentalSys_AnalysisBinning_H
#define runExperimentalSys_AnalysisBinning_H

#include <iostream>
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"

class AnalysisBinning{

 private:

  // Truth bins (STXS scheme with possible merging)

  // !!!!!! bbH bin dropped for now! !!!!!!

  enum STXS {GGH_FWDH=0, GGH_0j, GGH_1j_PTH_0_60, GGH_1j_PTH_60_120, GGH_1j_PTH_120_200, GGH_1j_PTH_GT200, GGH_2j_PTH_0_60, GGH_2j_PTH_60_120, GGH_2j_PTH_120_200, GGH_2j_PTH_GT200, GGH_VBFTOPO_JET3VETO, GGH_VBFTOPO_JET3, QQ2HQQ_FWDH, QQ2HQQ_VBFTOPO_JET3VETO, QQ2HQQ_VBFTOPO_JET3, QQ2HQQ_VH2JET, QQ2HQQ_REST, QQ2HQQ_PTJET1_GT200, QQ2HLNU_FWDH, QQ2HLNU_PTV_0_150, QQ2HLNU_PTV_150_250_0J, QQ2HLNU_PTV_150_250_ge1J, QQ2HLNU_PTV_GT250, QQ2HLL_FWDH, QQ2HLL_PTV_0_150, QQ2HLL_PTV_150_250_0J, QQ2HLL_PTV_150_250_GE1J, QQ2HLL_PTV_GT250, GG2HLL_FWDH, GG2HLL_PTV_0_150, GG2HLL_PTV_GT150_0J, GG2HLL_PTV_GT150_GE1J, TTH_FWDH, TTH, THJB_FWDH, THJB, TWH, BBH_FWDH, BBH, nSTXS};
  const std::vector<TString> m_STXS = {"GGH_0j_FWDH", "GGH_0j", "GGH_1j_PTH_0_60", "GGH_1j_PTH_60_120", "GGH_1j_PTH_120_200", "GGH_1j_PTH_GT200", "GGH_2j_PTH_0_60", "GGH_2j_PTH_60_120", "GGH_2j_PTH_120_200", "GGH_2j_PTH_GT200", "GGH_VBFTOPO_JET3VETO", "GGH_VBFTOPO_JET3", "QQ2HQQ_FWDH", "QQ2HQQ_VBFTOPO_JET3VETO", "QQ2HQQ_VBFTOPO_JET3", "QQ2HQQ_VH2JET", "QQ2HQQ_REST", "QQ2HQQ_PTJET1_GT200", "QQ2HLNU_FWDH", "QQ2HLNU_PTV_0_150", "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_150_250_ge1J", "QQ2HLNU_PTV_GT250", "QQ2HLL_FWDH", "QQ2HLL_PTV_0_150", "QQ2HLL_PTV_150_250_0J", "QQ2HLL_PTV_150_250_GE1J", "QQ2HLL_PTV_GT250", "GG2HLL_FWDH", "GG2HLL_PTV_0_150", "GG2HLL_PTV_GT150_0J", "GG2HLL_PTV_GT150_GE1J", "TTH_FWDH", "TTH", "THJB_FWDH", "THJB", "TWH", "bbH_FWDH", "bbH"};

  enum STXSstrongMerging {strong_GGH_0j=0, strong_GGH_1j_PTH_0_60, strong_GGH_1j_PTH_60_120, strong_GGH_1j_PTH_120_200, strong_GGH_2j, strong_GGH_BSM, strong_QQ2HQQ, strong_QQ2HQQ_BSM, strong_VHlep, strong_top, strong_bbH, strong_FWDH, nSTXSstrong};
  const std::vector<TString> m_STXSstrongMerging = {"GGH_0j", "GGH_1j_PTH_0_60", "GGH_1j_PTH_60_120", "GGH_1j_PTH_120_200", "GGH_2j", "GGH_BSM", "QQ2HQQ", "QQ2HQQ_BSM", "VHlep", "top", "bbH", "FWDH"};
  const std::vector<int> m_stage1ToStrong = {strong_FWDH, strong_GGH_0j, strong_GGH_1j_PTH_0_60, strong_GGH_1j_PTH_60_120, strong_GGH_1j_PTH_120_200, strong_GGH_BSM, strong_GGH_2j, strong_GGH_2j, strong_GGH_2j, strong_GGH_BSM, strong_GGH_2j, strong_GGH_2j, strong_FWDH, strong_QQ2HQQ, strong_QQ2HQQ, strong_QQ2HQQ, strong_QQ2HQQ, strong_QQ2HQQ_BSM, strong_FWDH, strong_VHlep, strong_VHlep, strong_VHlep, strong_VHlep, strong_FWDH, strong_VHlep, strong_VHlep, strong_VHlep, strong_VHlep, strong_FWDH, strong_VHlep, strong_VHlep, strong_VHlep, strong_FWDH, strong_top, strong_FWDH, strong_top, strong_top, strong_FWDH, strong_bbH};

  enum STXSweakMerging {weak_GGH_0j=0, weak_GGH_1j_PTH_0_60, weak_GGH_1j_PTH_60_120, weak_GGH_1j_PTH_120_200, weak_GGH_1j_BSM, weak_GGH_2j_PTH_0_60, weak_GGH_2j_PTH_60_120, weak_GGH_2j_PTH_120_200, weak_GGH_2j_BSM, weak_GGH_VBFlike, weak_QQ2HQQ_VBFlike, weak_QQ2HQQ_VHlike, weak_QQ2HQQ_BSM, weak_VHlep, weak_top, weak_bbH, weak_FWDH, nSTXSweak};
  const std::vector<TString> m_STXSweakMerging = {"GGH_0j", "GGH_1j_PTH_0_60", "GGH_1j_PTH_60_120", "GGH_1j_PTH_120_200", "GGH_1j_BSM", "GGH_2j_PTH_0_60", "GGH_2j_PTH_60_120", "GGH_2j_PTH_120_200", "GGH_2j_BSM", "GGH_VBFlike", "QQ2HQQ_VBFlike", "QQ2HQQ_VHlike", "QQ2HQQ_BSM", "VHlep", "top", "bbH", "FWDH"};
  const std::vector<int> m_stage1ToWeak = {weak_FWDH, weak_GGH_0j, weak_GGH_1j_PTH_0_60, weak_GGH_1j_PTH_60_120, weak_GGH_1j_PTH_120_200, weak_GGH_1j_BSM, weak_GGH_2j_PTH_0_60, weak_GGH_2j_PTH_60_120, weak_GGH_2j_PTH_120_200, weak_GGH_2j_BSM, weak_GGH_VBFlike, weak_GGH_VBFlike, weak_FWDH, weak_QQ2HQQ_VBFlike, weak_QQ2HQQ_VBFlike, weak_QQ2HQQ_VHlike, weak_QQ2HQQ_VHlike, weak_QQ2HQQ_BSM, weak_FWDH, weak_VHlep, weak_VHlep, weak_VHlep, weak_VHlep, weak_FWDH, weak_VHlep, weak_VHlep, weak_VHlep, weak_VHlep, weak_FWDH, weak_VHlep, weak_VHlep, weak_VHlep, weak_FWDH, weak_top, weak_FWDH, weak_top, weak_top, weak_FWDH, weak_bbH};


  // ATTENTION: no extra definiton of merging needed, as efficiencies and all systematics should be computed per production modes (i.e. running over separate files);
  // Therefore, all truth bins in the corresponding histogram can be merged, as they should be zero if a production mode in not contained in this bin from theory
  enum prodModes {prod_ggH=0, prod_VBF, prod_WH, prod_ZH, prod_ggZH, prod_ttH, prod_bbH, prod_tHjb, prod_tWH, nProdModes};
  const std::vector<TString> m_prodModes = {"ggH", "VBF", "WH", "ZH", "ggZH", "ttH", "bbH", "tHjb", "tWH"};
  enum minProdModes {minprod_ggH=0, minprod_VBF, minprod_VH, minprod_top, nMinProdModes};
  const std::vector<TString> m_minProdModes = {"ggH", "VBF", "VH", "top"};


  // Reco categories (might change)

  enum categories {ttHCP_Cate1=0, ttHCP_Cate2, ttHCP_Cate3, ttHCP_Cate4, ttHCP_Cate5, ttHCP_Cate6, ttHCP_Cate7, ttHCP_Cate8, ttHCP_Cate9, ttHCP_Cate10, ttHCP_Cate11, ttHCP_Cate12, ttHCP_Cate13, ttHCP_Cate14, ttHCP_Cate15, ttHCP_Cate16, ttHCP_Cate17, ttHCP_Cate18, ttHCP_Cate19, ttHCP_Cate20, nRecoCat};
  const std::vector<TString> m_categories = {"ttHCP_Cate1", "ttHCP_Cate2", "ttHCP_Cate3", "ttHCP_Cate4", "ttHCP_Cate5", "ttHCP_Cate6", "ttHCP_Cate7", "ttHCP_Cate8", "ttHCP_Cate9", "ttHCP_Cate10", "ttHCP_Cate11", "ttHCP_Cate12", "ttHCP_Cate13", "ttHCP_Cate14", "ttHCP_Cate15", "ttHCP_Cate16", "ttHCP_Cate17", "ttHCP_Cate18", "ttHCP_Cate19", "ttHCP_Cate20"};

  /* 
  enum categories {ggH_0J_CEN=0, ggH_0J_FWD, ggH_1J_LOW, ggH_1J_MED, ggH_1J_HIGH, ggH_1J_BSM, ggH_2J_LOW, ggH_2J_MED, ggH_2J_HIGH, ggH_2J_BSM, VBF_HjjLOW_loose, VBF_HjjLOW_tight, VBF_HjjHIGH_loose, VBF_HjjHIGH_tight, VHhad_loose, VHhad_tight, qqH_BSM, VHMET_LOW, VHMET_HIGH, VHlep_LOW, VHlep_HIGH, VHdilep, ttHHad_XGBoost4, ttHHad_XGBoost3, ttHHad_XGBoost2, ttHHad_XGBoost1, ttHLep_XGBoost3, ttHLep_XGBoost2, ttHLep_XGBoost1, nRecoCat};
  const std::vector<TString> m_categories = {"ggH_0J_CEN", "ggH_0J_FWD", "ggH_1J_LOW", "ggH_1J_MED", "ggH_1J_HIGH", "ggH_1J_BSM", "ggH_2J_LOW", "ggH_2J_MED", "ggH_2J_HIGH", "ggH_2J_BSM", "VBF_HjjLOW_loose", "VBF_HjjLOW_tight", "VBF_HjjHIGH_loose", "VBF_HjjHIGH_tight", "VHhad_loose", "VHhad_tight", "qqH_BSM", "VHMET_LOW", "VHMET_HIGH", "VHlep_LOW", "VHlep_HIGH", "VHdilep", "ttHHad_XGBoost4", "ttHHad_XGBoost3", "ttHHad_XGBoost2", "ttHHad_XGBoost1", "ttHLep_XGBoost3", "ttHLep_XGBoost2", "ttHLep_XGBoost1"};
  */

 public:
  AnalysisBinning();
  virtual ~AnalysisBinning();

  int getNTruthBin(TString merging="stage1"); // number of POI in given merging scheme
  int getTruthBinStage1(int binMxAOD, bool isTWH=false); // truth bin number in stage 1 scheme given certain bin in MxAOD branch HTXS_Stage1_Category_pTjet30
  int getTruthBin(int binStage1, TString merging="stage1"); // truth bin in certain merging scheme, in which falls a certain stage 1 scheme
  TString getTruthBinName(int bin, TString merging="stage1"); // name of truth bin in given merging scheme
 
  int getNRecoCat(){ return nRecoCat; }
  int getRecoCat(int catMxAOD);
  TString getRecoCatName(int cat){ return m_categories[cat]; }

  TH1F get1DHistWithMerging(TH1F *h, TString merging="stage1", TString productionMode="");
  TH2F get2DHistWithMerging(TH2F *h, TString merging="stage1", TString productionMode="");

  std::vector<TString> getProductionModes(){ return m_prodModes; }


 private:

  std::vector<int> getMergingRecipe(TString merging, TString productionMode);

};

#endif // runExperimentalSys_AnalysisBinning_H
