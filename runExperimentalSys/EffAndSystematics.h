#ifndef runExperimentalSys_EffAndSystematics_H
#define runExperimentalSys_EffAndSystematics_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"

class AnalysisBinning;
class EffAndSystematics : public HgammaAnalysis{
  
 public:

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
 private:

  const std::vector<TString> NP_QCDggH = {"mu","res","mig01","mig12","vbf2j","vbf3j","pTH60","pTH120","qm_t"};
  const double m_lumi_1516 = 36.2077; // fb-1
  const double m_lumi_17   = 43.813; // fb-1
  const double m_lumi_18   = 59.9372; // fb-1
  double m_lumiWeight; //!  
  double m_lumi; //!  
  int counter = 0; //!

  std::vector<CP::SystematicSet> availableSys;
  bool m_expSystematics; //!
  bool m_theoSystematics; //!
  int m_currentID; //!
  bool m_isGGH; //!
  bool m_isVBF; //!
  bool m_isVH; //!
  bool m_isTWH; //!
  bool m_isTTH; //!
  bool m_isTHJB; //!
  bool m_isBBH; //!

  TTree *m_outTree; //!
  Float_t m_pTH; //!
  Float_t m_pTH_truth; //!
  Float_t m_pTHjj; //!
  Float_t m_pTHjj_truth; //!
  Int_t m_Nj; //!
  Int_t m_Nj_truth; //!
  Int_t m_cat; //!
  Int_t m_bin; //!
  Float_t m_wNom; //!
  Float_t m_wQCD_mu; //!
  Float_t m_wQCD_res; //!
  Float_t m_wQCD_mig01; //!
  Float_t m_wQCD_mig12; //!
  Float_t m_wQCD_vbf2j; //!
  Float_t m_wQCD_vbf3j; //!
  Float_t m_wQCD_pTH60; //!
  Float_t m_wQCD_pTH120; //!
  Float_t m_wQCD_qm_t; //!
 
  AnalysisBinning *m_binning; //!

 public:
  // this is a standard constructor
  EffAndSystematics() { }
  EffAndSystematics(const char *name);
  virtual ~EffAndSystematics();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode fileExecute();

  // this is needed to distribute the algorithm to the workers
  ClassDef(EffAndSystematics, 1);
};

#endif // runExperimentalSys_EffAndSystematics_H
