#include "runExperimentalSys/EffAndSystematics.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "runExperimentalSys/AnalysisBinning.h"

// this is needed to distribute the algorithm to the workers
ClassImp(EffAndSystematics)



EffAndSystematics::EffAndSystematics(const char *name) : HgammaAnalysis(name){
  m_currentID = 0;
  m_binning = new AnalysisBinning();
}



EffAndSystematics::~EffAndSystematics(){
  // delete m_binning;
  // TO DO: solve this problem!
}



EL::StatusCode EffAndSystematics::createOutput(){

  // Total weight of events and only noDalitz events for reweighting of efficiency
  histoStore()->createTH1F("totalEvts",1,0,2);
  histoStore()->createTH1F("totalEvtsNoDalitz",1,0,2);
  histoStore()->createTH1F("totalSumWeights",3,0,3);

  // Nominal histograms
  //histoStore()->createTH1F("Nt_nominal", m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
  //histoStore()->createTH2F("Nrt_nominal", m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
  histoStore()->createTH2F("Nrt_nominal", m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), 1, 0, 1);
  // m_yy vs category nominal histograms
  histoStore()->createTH2F("m_yy_nominal", m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), 550, 105, 160);
  std::cout<<"truth bin: "<<m_binning->getNTruthBin()<<std::endl;
  //-----------------------------------------------------------------

  // Experimental systematics
  m_expSystematics = config()->getBool("UseExperimentalUncert", false);
  //std::cout<<"m_expSystematics: "<<m_expSystematics<<std::endl;
  if(m_expSystematics){
    Info("createOutput()","Use experimental uncertainties.");

    // Get all uncertainties present in this MxAOD
    for (auto sys : getSystematics()){
      if(!isSystematicAvailable(sys)) continue;
      else if(sys.name()=="") continue;
      //std::cout<<"Get sys. name: "<<TString(sys.name()).Data()<<std::endl;
      availableSys.push_back(sys);
      //histoStore()->createTH2F(Form("Nrt_%s",TString(sys.name()).Data()), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
      histoStore()->createTH2F(Form("Nrt_%s",TString(sys.name()).Data()), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), 1, 0, 1);
      // m_yy vs category for systematic histograms
      histoStore()->createTH2F(Form("m_yy_%s",TString(sys.name()).Data()), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), 550, 105, 160);
    }

  }

  //-----------------------------------------------------------------

  // Theory uncertainties
  m_theoSystematics = config()->getBool("UseTheoryUncert", false);
  //std::cout<<"m_theoSystematics: "<<m_theoSystematics<<std::endl;
  if(m_theoSystematics){
    Info("createOutput()","Use theoretical uncertainties.");

    // General QCD scale uncertainties
    TruthWeightTools::HiggsWeights hw = eventHandler()->higgsWeights();
    for(int i=0; i<hw.qcd.size(); i++){
      histoStore()->createTH1F(Form("Nt_QCDscale_%i",i), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
      histoStore()->createTH2F(Form("Nrt_QCDscale_%i",i), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    }

    // Elaborated QCD scale uncertainty scheme for NNLOPS ggH
    for(TString NP : NP_QCDggH){
      histoStore()->createTH1F(Form("Nt_QCDscale_ggH_%s",NP.Data()), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
      histoStore()->createTH2F(Form("Nrt_QCDscale_ggH_%s",NP.Data()), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    }

    // PDF uncertainties
    for(int i=0; i<30; ++i){
      histoStore()->createTH1F(Form("Nt_PDF4LHC_NLO_30_EV%i",i+1), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
      histoStore()->createTH2F(Form("Nrt_PDF4LHC_NLO_30_EV%i",i+1), m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    }

    // alphaS uncertainties
    histoStore()->createTH1F("Nt_PDF4LHC_NLO_30_alphaS_up",  m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    histoStore()->createTH1F("Nt_PDF4LHC_NLO_30_alphaS_down",m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    histoStore()->createTH2F("Nrt_PDF4LHC_NLO_30_alphaS_up",  m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());
    histoStore()->createTH2F("Nrt_PDF4LHC_NLO_30_alphaS_down",m_binning->getNRecoCat(), 0, m_binning->getNRecoCat(), m_binning->getNTruthBin(), 0, m_binning->getNTruthBin());

  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode EffAndSystematics::fileExecute(){
  HgammaAnalysis::fileExecute();

  TString sampleName = wk()->inputFile()->GetName();
  std::cout<<"sampleName: "<<sampleName<<std::endl;

  m_isGGH = sampleName.Contains("ggH125");
  m_isVBF = sampleName.Contains("VBFH125");
  m_isVH = sampleName.Contains("WpH125") || sampleName.Contains("WmH125") || sampleName.Contains("ZH125") || sampleName.Contains("ggZH125");
  m_isTWH = sampleName.Contains("tWH");
  m_isTTH = sampleName.Contains("ttH");
  m_isTHJB= sampleName.Contains("tHjb");
  m_isBBH = sampleName.Contains("bbH125");

  if(sampleName.Contains("mc16a")) m_lumi = m_lumi_1516;
  if(sampleName.Contains("mc16d")) m_lumi = m_lumi_17;
  if(sampleName.Contains("mc16e")) m_lumi = m_lumi_18;
  
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode EffAndSystematics::execute(){
  
  HgammaAnalysis::execute();
  histoStore()->fillTH1F("totalEvts",1,var::weightInitial());
  if(!var::isDalitzEvent()) histoStore()->fillTH1F("totalEvtsNoDalitz",1,var::weightInitial());

  ULong64_t eventNumber = eventInfo()->auxdataConst<unsigned long long>("eventNumber");
  int HTXS_Stage1;

  //std::cout<<"eventNumber: "<<eventNumber<<std::endl;
  //std::cout<<"m_isTTH: "<<m_isTTH<<std::endl;

  if(m_isTTH || m_isTWH || m_isTHJB) {
     if((int)eventNumber%4 != 3) return EL::StatusCode::SUCCESS;
  } 
  /*
  // Uggly fix for broken MxAODs
  if(m_isTTH){
    if(eventInfo()->auxdataConst<unsigned long long>("eventNumber")%100<80) return EL::StatusCode::SUCCESS;
    if(var::yAbs_yy.truth()<2.5) HTXS_Stage1 = 601;
    else HTXS_Stage1 = 600;
  }
  else if(m_isTHJB || m_isTWH){
    if(var::yAbs_yy.truth()<2.5) HTXS_Stage1 = 801;
    else HTXS_Stage1 = 800;
  }
  else if(m_isBBH){
    if(var::yAbs_yy.truth()<2.5) HTXS_Stage1 = 701;
    else HTXS_Stage1 = 700;
  }
  else{
    HTXS_Stage1 = eventInfo()->auxdata<int>("HTXS_Stage1_Category_pTjet30");
    if(eventInfo()->auxdata<int>("HTXS_errorCode")!=0) return EL::StatusCode::SUCCESS;
  }
  */
  //HTXS_Stage1 = eventInfo()->auxdata<int>("HTXS_Stage1_Category_pTjet30");
  //std::cout<<"HTXS_Stage1: "<<HTXS_Stage1<<std::endl;
  //if(eventInfo()->auxdata<int>("HTXS_errorCode")!=0) return EL::StatusCode::SUCCESS;

  //int bin = m_binning->getTruthBinStage1(HTXS_Stage1, m_isTWH);
  int cat = m_binning->getRecoCat(var::catCoup_XGBoost_ttHCP());
  //std::cout<<"bin: "<<bin<<" cat: "<<cat<<std::endl;

  m_lumiWeight = lumiXsecWeight(m_lumi);
  double sumInitial = getIntialSumOfWeights(-1);
  if (counter == 0) {
     if (m_lumi < 40.) histoStore()->fillTH1F("totalSumWeights", 0.5, sumInitial);
     else if (m_lumi < 50.) histoStore()->fillTH1F("totalSumWeights", 1.5, sumInitial);
     else if (m_lumi < 60.) histoStore()->fillTH1F("totalSumWeights", 2.5, sumInitial);
  }
  double TotalSumWt = 1.;
  if (m_isTTH){
      if (m_lumi < 40.) TotalSumWt = 858858.601562; 
      else if (m_lumi < 50.) TotalSumWt = 1075384.59229;
      else if (m_lumi < 60.) TotalSumWt = 1418637.31201;
      //else if (m_lumi < 50.) TotalSumWt = 720980.851074;
      //else if (m_lumi < 60.) TotalSumWt = 1410256.70459;
  }
  if (counter == 0) std::cout<<"TotalSumWt: "<<TotalSumWt<<std::endl;
  //std::cout<<"sumInitial: "<<sumInitial<<" m_isTTH: "<<m_isTTH<<std::endl;
  if(m_isTTH || m_isTWH || m_isTHJB) m_lumiWeight *= 4.; // To exclude events used in the ttH CP BDT training
  counter++;
  m_lumiWeight = m_lumiWeight/TotalSumWt;
  double w_ini = var::weightInitial() * m_lumiWeight;
  double w_nom = var::weightCatCoup_XGBoost_ttHCP() * m_lumiWeight;
  //-----------------------------------------------------------------

  // Nominal histograms
  //histoStore()->fillTH1F("Nt_nominal", bin, w_ini);
  //if(var::isPassed()) histoStore()->fillTH2F("Nrt_nominal", cat, bin, w_nom);
  if(var::isPassed()) histoStore()->fillTH2F("Nrt_nominal", cat, 0.5, w_nom);
  if(var::isPassed()) histoStore()->fillTH2F("m_yy_nominal", cat, var::m_yy()*0.001, w_nom);

  // Experimental uncertainties
  if(m_expSystematics){
    for(auto sys : availableSys){
      //std::cout<<"Loop sys. name: "<<TString(sys.name()).Data()<<std::endl;
      HG::VarHandler::getInstance()->applySystematicVariation(sys);
  // applySystematicVariation(sys);
      //int catVar = m_binning->getRecoCat(var::catCoup_XGBoost_ttH());
      int catVar = m_binning->getRecoCat(var::catCoup_XGBoost_ttHCP());
      double wVar = var::weightCatCoup_XGBoost_ttHCP() * m_lumiWeight;
      if(var::isPassed()){
	TString name = sys.name();
	//histoStore()->fillTH2F(Form("Nrt_%s",name.Data()), catVar, bin, wVar);
	histoStore()->fillTH2F(Form("Nrt_%s",name.Data()), catVar, 0.5, wVar);
	histoStore()->fillTH2F(Form("m_yy_%s",name.Data()), catVar, var::m_yy()*0.001, wVar);
      }
    }
    applySystematicVariation(CP::SystematicSet());
  }

  return EL::StatusCode::SUCCESS;
}

