#include <runExperimentalSys/EffAndSystematics.h>
#include <runExperimentalSys/AnalysisBinning.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif


#ifdef __CINT__
#pragma link C++ class AnalysisBinning+;
#endif

#ifdef __CINT__
#pragma link C++ class EffAndSystematics+;
#endif
