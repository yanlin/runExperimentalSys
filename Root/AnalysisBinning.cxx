#include "runExperimentalSys/AnalysisBinning.h"

AnalysisBinning::AnalysisBinning(){}

AnalysisBinning::~AnalysisBinning(){}

int AnalysisBinning::getRecoCat(int catMxAOD){ 

  int recoCatMatch[21];
  recoCatMatch[0] = -999;
  recoCatMatch[1] = ttHCP_Cate1; 
  recoCatMatch[2] = ttHCP_Cate2;
  recoCatMatch[3] = ttHCP_Cate3; 
  recoCatMatch[4] = ttHCP_Cate4; 
  recoCatMatch[5] = ttHCP_Cate5; 
  recoCatMatch[6] = ttHCP_Cate6; 
  recoCatMatch[7] = ttHCP_Cate7; 
  recoCatMatch[8] = ttHCP_Cate8; 
  recoCatMatch[9] = ttHCP_Cate9; 
  recoCatMatch[10] = ttHCP_Cate10; 
  recoCatMatch[11] = ttHCP_Cate11; 
  recoCatMatch[12] = ttHCP_Cate12; 
  recoCatMatch[13] = ttHCP_Cate13; 
  recoCatMatch[14] = ttHCP_Cate14; 
  recoCatMatch[15] = ttHCP_Cate15; 
  recoCatMatch[16] = ttHCP_Cate16; 
  recoCatMatch[17] = ttHCP_Cate17; 
  recoCatMatch[18] = ttHCP_Cate18; 
  recoCatMatch[19] = ttHCP_Cate19; 
  recoCatMatch[20] = ttHCP_Cate20; 

  if(catMxAOD<0 || catMxAOD>20) return -999;
  return recoCatMatch[catMxAOD]; 
}

/*
int AnalysisBinning::getRecoCat(int catMxAOD){ 

  int recoCatMatch[34];
  recoCatMatch[0] = -999;
  recoCatMatch[1] = ggH_0J_CEN;
  recoCatMatch[2] =  ggH_0J_FWD;
  recoCatMatch[3] =  ggH_1J_LOW; 
  recoCatMatch[4] = ggH_1J_MED; 
  recoCatMatch[5] = ggH_1J_HIGH; 
  recoCatMatch[6] = ggH_1J_BSM; 
  recoCatMatch[7] = ggH_2J_LOW; 
  recoCatMatch[8] = ggH_2J_MED; 
  recoCatMatch[9] = ggH_2J_HIGH; 
  recoCatMatch[10] = ggH_2J_BSM; 
  recoCatMatch[11] = VBF_HjjLOW_loose; 
  recoCatMatch[12] = VBF_HjjLOW_tight; 
  recoCatMatch[13] = VBF_HjjHIGH_loose; 
  recoCatMatch[14] = VBF_HjjHIGH_tight; 
  recoCatMatch[15] = VHhad_loose; 
  recoCatMatch[16] = VHhad_tight; 
  recoCatMatch[17] = qqH_BSM; 
  recoCatMatch[18] = VHMET_LOW; 
  recoCatMatch[19] = VHMET_HIGH; 
  recoCatMatch[20] = VHMET_HIGH; 
  recoCatMatch[21] = VHlep_LOW; 
  recoCatMatch[22] = VHlep_HIGH; 
  recoCatMatch[23] = VHdilep; 
  recoCatMatch[24] = VHdilep; 
  recoCatMatch[25] = -999;
  recoCatMatch[26] = -999; 
  recoCatMatch[27] = ttHHad_XGBoost4; 
  recoCatMatch[28] = ttHHad_XGBoost3; 
  recoCatMatch[29] = ttHHad_XGBoost2; 
  recoCatMatch[30] = ttHHad_XGBoost1; 
  recoCatMatch[31] = ttHLep_XGBoost3; 
  recoCatMatch[32] = ttHLep_XGBoost2; 
  recoCatMatch[33] = ttHLep_XGBoost1;

  if(catMxAOD<0 || catMxAOD>33) return -999;
  return recoCatMatch[catMxAOD]; 
}
*/

int AnalysisBinning::getTruthBinStage1(int binMxAOD, bool isTWH){

  if(isTWH && binMxAOD==801) binMxAOD += 1;

  int truthBinMatch[1000];
  for(int i=0; i<1000; i++) truthBinMatch[i] = -999;
  truthBinMatch[100] = GGH_FWDH;
  truthBinMatch[103] = GGH_0j;
  truthBinMatch[104] = GGH_1j_PTH_0_60; 
  truthBinMatch[105] = GGH_1j_PTH_60_120; 
  truthBinMatch[106] = GGH_1j_PTH_120_200; 
  truthBinMatch[107] = GGH_1j_PTH_GT200; 
  truthBinMatch[108] = GGH_2j_PTH_0_60; 
  truthBinMatch[109] = GGH_2j_PTH_60_120; 
  truthBinMatch[110] = GGH_2j_PTH_120_200; 
  truthBinMatch[111] = GGH_2j_PTH_GT200; 
  truthBinMatch[101] = GGH_VBFTOPO_JET3VETO; 
  truthBinMatch[102] = GGH_VBFTOPO_JET3; 
  truthBinMatch[200] = QQ2HQQ_FWDH; 
  truthBinMatch[201] = QQ2HQQ_VBFTOPO_JET3VETO; 
  truthBinMatch[202] = QQ2HQQ_VBFTOPO_JET3; 
  truthBinMatch[203] = QQ2HQQ_VH2JET; 
  truthBinMatch[204] = QQ2HQQ_REST; 
  truthBinMatch[205] = QQ2HQQ_PTJET1_GT200; 
  truthBinMatch[300] = QQ2HLNU_FWDH; 
  truthBinMatch[301] = QQ2HLNU_PTV_0_150; 
  truthBinMatch[302] = QQ2HLNU_PTV_150_250_0J; 
  truthBinMatch[303] = QQ2HLNU_PTV_150_250_ge1J; 
  truthBinMatch[304] = QQ2HLNU_PTV_GT250; 
  truthBinMatch[400] = QQ2HLL_FWDH; 
  truthBinMatch[401] = QQ2HLL_PTV_0_150; 
  truthBinMatch[402] = QQ2HLL_PTV_150_250_0J; 
  truthBinMatch[403] = QQ2HLL_PTV_150_250_GE1J; 
  truthBinMatch[404] = QQ2HLL_PTV_GT250; 
  truthBinMatch[500] = GG2HLL_FWDH; 
  truthBinMatch[501] = GG2HLL_PTV_0_150; 
  truthBinMatch[502] = GG2HLL_PTV_GT150_0J; 
  truthBinMatch[503] = GG2HLL_PTV_GT150_GE1J; 
  truthBinMatch[600] = TTH_FWDH; 
  truthBinMatch[601] = TTH; 
  truthBinMatch[700] = BBH_FWDH;
  truthBinMatch[701] = BBH;
  truthBinMatch[800] = THJB_FWDH;
  truthBinMatch[801] = THJB;
  truthBinMatch[802] = TWH; // !!! 802 not written in MxAOD; set by hand according to production mode

  if(binMxAOD<0 || binMxAOD>802) return -999;
  return truthBinMatch[binMxAOD]; 
}



int AnalysisBinning::getNTruthBin(TString merging){ 
  if(merging=="stage1") return nSTXS;
  else if(merging=="strong") return nSTXSstrong;
  else if(merging=="weak") return nSTXSweak;
  else if(merging=="production" or merging=="fiducialProdXS") return nProdModes;
  else if(merging=="minimal") return nMinProdModes;
  else std::cout << "ERROR: merging scheme " << merging << " does not exist." << std::endl;
  return -1;
}

int AnalysisBinning::getTruthBin(int binStage1, TString merging){
  if(merging=="stage1") return binStage1; 
  else if(merging=="strong") return m_stage1ToStrong[binStage1];
  else if(merging=="weak") return m_stage1ToWeak[binStage1];
  else if(merging=="production" || merging=="minimal" || merging=="fiducialProdXS"){
    std::cout << "WARNING: There are several production modes per truth bin in stage 1; please get this information from the sample you are using" << std::endl;
    return 0;
  }
  else std::cout << "ERROR: merging scheme " << merging << " does not exist." << std::endl;
  return -1;
}

TString AnalysisBinning::getTruthBinName(int bin, TString merging){
  if(merging=="stage1") return m_STXS[bin]; 
  else if(merging=="strong") return m_STXSstrongMerging[bin];
  else if(merging=="weak") return m_STXSweakMerging[bin];
  else if(merging=="production" || merging=="fiducialProdXS") return m_prodModes[bin];
  else if(merging=="minimal") return m_minProdModes[bin];
  else std::cout << "ERROR: merging scheme " << merging << " does not exist." << std::endl;
  return "UNKNOWN";
}


TH1F AnalysisBinning::get1DHistWithMerging(TH1F *h, TString merging, TString productionMode){
  int nBins = getNTruthBin(merging);
  std::vector<int> relation = getMergingRecipe(merging, productionMode);

  TH1F h_merged(Form("%s_merged",h->GetName()),"",nBins,0,nBins);
  for(int i=0; i<nSTXS; i++) h_merged.AddBinContent(relation[i]+1,h->GetBinContent(i+1));

  return h_merged;
}


TH2F AnalysisBinning::get2DHistWithMerging(TH2F *h, TString merging, TString productionMode){
  int nBins = getNTruthBin(merging);
  std::vector<int> relation = getMergingRecipe(merging, productionMode);

  TH2F h_merged(Form("%s_merged",h->GetName()),"",h->GetNbinsX(), 0, h->GetNbinsX(), nBins,0,nBins);
  for(int b=0; b<h->GetNbinsX(); b++){
    for(int i=0; i<nSTXS; i++) h_merged.AddBinContent(h_merged.GetBin(b+1,relation[i]+1),h->GetBinContent(b+1,i+1));
  }

  return h_merged;
}


std::vector<int> AnalysisBinning::getMergingRecipe(TString merging, TString productionMode){

  std::vector<int> relation; 
  for(int i=0; i<nSTXS; i++) relation.push_back(i);

  // Strong merging scheme of STXS truth bins
  if(merging=="strong") relation = m_stage1ToStrong;

  // Strong merging scheme of STXS truth bins
  else if(merging=="weak") relation = m_stage1ToWeak;

  // Total production mode cross section
  else if(merging=="production"){
    if(productionMode.Length()==0) std::cout << "Please provide the production mode" << std::endl;
    else{
      int index = -1;
      for(int i=0; i<m_prodModes.size(); i++){
	if(m_prodModes[i]==productionMode) index = i;
      }
      for(int i=0; i<nSTXS; i++) relation[i] = index;
    }
  }

  // Production modes cross section in fiducial phase space of detector acceptance: |y|<2.5
  else if(merging=="fiducialProdXS"){
    if(productionMode.Length()==0) std::cout << "Please provide the production mode" << std::endl;
    else{
      int index = -1;
      for(int i=0; i<m_prodModes.size(); i++){
	if(m_prodModes[i]==productionMode) index = i;
      }
      for(int i=0; i<nSTXS; i++){
	if(m_STXS[i].EndsWith("_FWDH")) relation[i] = -1;
	else relation[i] = index;
      }
    }
  }

  else{
    if(merging!="stage1") std::cout << "ERROR: merging scheme " << merging << " can not be computed from the input." << std::endl;
  }

  return relation;

}
