#!/bin/bash

SourceCode=/afs/cern.ch/work/y/yanlin/HyyAnalysis/2019.12.16_ttHCPExpUnc/HGam_h024/source
cd $SourceCode

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AnalysisBase,21.2.56,here
source ../build/x86_64-centos7-gcc62-opt/setup.sh

config='runExperimentalSys/EffAndSystematics.cfg'
BaseConfig='HGamAnalysisFramework/HGamRel21.config'

inputFileList=$2
OutputDir=$1

runEffAndSystematics $config InputFileList: $inputFileList BaseConfig: $BaseConfig OutputDir: $OutputDir UseExperimentalUncert: YES UseTheoryUncert: NO
