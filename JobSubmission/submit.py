#!/usr/bin/python2.6

import sys, os
import shutil
import getpass
import glob,fileinput

current=os.getcwd()
output="/afs/cern.ch/work/y/yanlin/public/ttHCP_0214"
#output="/afs/cern.ch/work/y/yanlin/HyyAnalysis/2019.12.16_ttHCPExpUnc/HGam/source/runExperimentalSys"
outfolder_evnt=output+"/FlavorSys"

if not os.path.isdir(outfolder_evnt): os.mkdir(outfolder_evnt)

basecondor=current+"/runExperimentalSys.sh"

finput = open(sys.argv[1], "r")

for line in finput.readlines():

    line = line.rstrip("\n")
    dirname = line.split("/")[-1].split(".")[0]
    print dirname
  
    datasetout=outfolder_evnt+"/{0}".format(dirname)
    if not os.path.isdir(datasetout): os.mkdir(datasetout)  
    os.chdir(datasetout)

    dataset = open(line, "r")
    index = 0

    for File in dataset.readlines():
	File = File.rstrip("\n")
	print File
	subdir = "subdir_"+str(index)
        index = index+1        
   
        fileout=datasetout+"/{0}".format(subdir)
	if not os.path.isdir(fileout): os.mkdir(fileout)  
	os.chdir(fileout)
  
        os.system("echo {0} >> input.txt".format(File))
	os.system("cp {0} .".format(basecondor))

	condorscript="{0}".format("MyCondor")
	fcondor=open(condorscript,"w")
	fcondor.write("Executable ={0}/{1}\n".format(fileout, basecondor.split("/")[-1]))
	fcondor.write("Universe = vanilla\n")
	fcondor.write("Notification = never\n")
	fcondor.write("\n")
	fcondor.write("should_transfer_files = YES\n")
	fcondor.write("when_to_transfer_output = ON_EXIT\n")
	fcondor.write("Output = {0}/condor.out\n".format(fileout))
	fcondor.write("Error  = {0}/condor.err\n".format(fileout))
	fcondor.write("Log    = {0}/condor.log\n".format(fileout))
	fcondor.write('stream_output  = True\n')
	fcondor.write('stream_error   = True\n') 
	fcondor.write("transfer_input_files =\n")
	#fcondor.write("+JobFlavour  = \"longlunch\"\n")
        fcondor.write("+JobFlavour  = \"microcentury\"\n")
	fcondor.write("Arguments ={0}/output {0}/input.txt\n".format(fileout))
	fcondor.write("Queue\n")
	fcondor.close()
	os.system("chmod +x {0}/*.sh".format(fileout))
	os.system("condor_submit {0}/{1}".format(fileout,condorscript))    
os.chdir(current)
