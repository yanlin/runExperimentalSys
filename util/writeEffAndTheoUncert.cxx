#include <iostream>
#include <fstream>
#include <cmath>
#include "TFile.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"

#include "runExperimentalSys/AnalysisBinning.h"

int main(int argc, char *argv[]){

  int opt; 
  extern char *optarg;

  TString inputFile="";
  TString outputFolder="YamlFiles";
  TString merging="stage1";
  TString mode = "ALL";
  double pruning=0.;

  while((opt = getopt(argc, argv, "i:o:m:p:x:h")) != -1){
    switch(opt){
    case 'i':
      inputFile = TString(optarg);
      break;
    case 'o':
      outputFolder = TString(optarg);
      break;
    case 'm':
      merging = TString(optarg);
      break;
    case 'p':
      mode = TString(optarg);
      break;
    case 'x':
      pruning = atof(optarg);
      break;
    case 'h':
      std::cout << "i: name of input file; put ? as place holder for production mode" << std::endl;
      std::cout << "o: name of output folder" << std::endl;
      std::cout << "m: merging (default: stage1, other options: strong, weak, production, fiducialProdXS)" << std::endl;
      std::cout << "p: only a single production mode" << std::endl;
      std::cout << "x: pruning (default: no pruning)" << std::endl;
      return 0;
    }
  }

  if(inputFile.Length()==0 || !inputFile.EndsWith(".root")){
    std::cout << "Please provide an input file in .root format (containing histograms)" << std::endl;
    return -1;
  }

  AnalysisBinning binning;
  std::vector<TString> prodModes;
  if(mode=="ALL") prodModes =  binning.getProductionModes();
  else prodModes = {mode};
  std::cout << "Run over " << prodModes.size() << " production modes" << std::endl;

  // Output Yaml files
  std::ofstream effFile;
  effFile.open(outputFolder+"/efficiencies_"+merging+".yaml");
  std::ofstream QCDscaleFile;
  QCDscaleFile.open(outputFolder+"/QCDscaleSys_"+merging+".yaml");
  std::ofstream PdfAndAlphaSFile;
  PdfAndAlphaSFile.open(outputFolder+"/PdfAndAlphaSSys_"+merging+".yaml");


  for(TString prodMode : prodModes){
    std::cout << prodMode << std::endl;

    TString inFile = inputFile.Copy();
    inFile.ReplaceAll("?",prodMode);
    TFile f_in(inFile);

    TH1F h_Nt_nominal = binning.get1DHistWithMerging((TH1F*)f_in.Get("Nt_nominal"), merging, prodMode);
    TH2F h_Nrt_nominal = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_nominal"), merging, prodMode);
    std::cout << "Found nominal histograms" << std::endl;

    double totEvts = ((TH1F*)f_in.Get("totalEvts"))->GetBinContent(1);
    double totEvtsNoDalitz = ((TH1F*)f_in.Get("totalEvtsNoDalitz"))->GetBinContent(1);
    double DalitzCorrect = totEvts / totEvtsNoDalitz;
    std::cout << "Calculated correction to total number of events (including Dalitz events)" << std::endl;

    //===========================================================================================
    // nominal values of efficiency matrix
    std::cout << "Writing nominal efficiencies..." << std::endl;
    effFile << prodMode << ":" << std::endl;

    for(int bin=0; bin<binning.getNTruthBin(merging); bin++){
      effFile << std::string(2,' ') << binning.getTruthBinName(bin,merging) << ":" << std::endl;
      for(int cat=0; cat<binning.getNRecoCat(); cat++){
	double eff = h_Nrt_nominal.GetBinContent(cat+1,bin+1) / h_Nt_nominal.GetBinContent(bin+1) * DalitzCorrect;
	if(h_Nt_nominal.GetBinContent(bin+1)==0) eff = 0;
	effFile << std::string(4,' ') << binning.getRecoCatName(cat) << ": " << eff << std::endl;
      }
    }

    std::cout << "done." << std::endl;

    // For the others, no detailed theory systematics available
    if(prodMode!="ggH" && prodMode!="VBF" && prodMode!="WH" && prodMode!="ZH" && prodMode!="ggZH" && prodMode!="ttH") continue;


    //===========================================================================================
    // QCD scale uncertinaties
    std::cout << "Writing QCD scale uncertainties..." << std::endl;
    QCDscaleFile << prodMode << ":" << std::endl;

    std::vector<TString> NPs;
    if(prodMode=="ggH")
      NPs = {
	"QCDscale_ggH_mu",
	"QCDscale_ggH_res",
	"QCDscale_ggH_mig01",
	"QCDscale_ggH_mig12",
	"QCDscale_ggH_vbf2j",
	"QCDscale_ggH_vbf3j",
	"QCDscale_ggH_pTH60",
	"QCDscale_ggH_pTH120",
	"QCDscale_ggH_qm_t"
      };
    else if(prodMode=="WH" || prodMode=="ZH" || prodMode=="ggZH") NPs = {"QCDscale_VH"};
    else NPs = {"QCDscale_"+prodMode};

    for(int bin=0; bin<binning.getNTruthBin(merging); bin++){
      bool binWritten = false;
      for(int cat=0; cat<binning.getNRecoCat(); cat++){

	// Nominal values
	double Nrt_nom = h_Nrt_nominal.GetBinContent(cat+1,bin+1);
	double Nt_nom = h_Nt_nominal.GetBinContent(bin+1);
	double eff_nom = Nrt_nom==0 ? 0 : Nrt_nom / Nt_nom;
	// Don't write uncertainties for bins with 0 efficiency
	if(eff_nom==0) continue;

	if(!binWritten) QCDscaleFile << std::string(2,' ') << binning.getTruthBinName(bin,merging) << ":" << std::endl;
	binWritten = true;
	QCDscaleFile << std::string(4,' ') << binning.getRecoCatName(cat) << ":" << std::endl;

	for(TString NP : NPs){ 

	  double Nrt_var=Nrt_nom, Nt_var=Nt_nom, eff_var=eff_nom;

	  if(prodMode=="ggH"){
	    TH2F h_Nrt_var = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP),merging, prodMode);
	    Nrt_var = h_Nrt_var.GetBinContent(cat+1,bin+1);
	    TH1F h_Nt_var = binning.get1DHistWithMerging((TH1F*)f_in.Get("Nt_"+NP),merging, prodMode);
	    Nt_var = h_Nt_var.GetBinContent(bin+1);
	  }
	  else{
	    int nQCDvar=8;
	    if(prodMode=="VBF") nQCDvar=6;
	    std::vector<double> yieldVars, truthVars;
	    for(int i=0; i<nQCDvar; i++){
	      TH2F h_Nrt_var = binning.get2DHistWithMerging((TH2F*)f_in.Get(Form("Nrt_QCDscale_%i",i)),merging, prodMode);
	      TH1F h_Nt_var = binning.get1DHistWithMerging((TH1F*)f_in.Get(Form("Nt_QCDscale_%i",i)),merging, prodMode);
	      if(fabs(h_Nrt_var.GetBinContent(cat+1,bin+1)-Nrt_nom)>fabs(Nrt_var-Nrt_nom)) Nrt_var = h_Nrt_var.GetBinContent(cat+1,bin+1);
	      if(fabs(h_Nt_var.GetBinContent(cat+1,bin+1)-Nt_nom)>fabs(Nt_var-Nt_nom)) Nt_var = h_Nt_var.GetBinContent(bin+1);
	      double effv = h_Nrt_var.GetBinContent(cat+1,bin+1)==0 ? 0 : h_Nrt_var.GetBinContent(cat+1,bin+1)/h_Nt_var.GetBinContent(bin+1);
	      if(fabs(effv-eff_nom)>fabs(eff_var-eff_nom)) eff_var = effv;
	    }
	  }
	  

	  // double eff_var = Nrt_var==0 ? 0 : Nrt_var / Nt_var;

	  // Write datacard
	  double eff_relVar = eff_nom==0 ? 0 : (eff_var-eff_nom) / eff_nom;
	  double Nrt_relVar = Nrt_nom==0 ? 0 : (Nrt_var-Nrt_nom) / Nrt_nom;
	  double Nt_relVar = Nrt_nom==0 ? 0 : (Nt_var-Nt_nom) / Nt_nom;

	  // Since taking just the enveloppe of the variations, have to remove rel. sign in categories
	  if(prodMode!="ggH"){
	    eff_relVar = fabs(eff_relVar);
	    Nrt_relVar = fabs(Nrt_relVar);
	    Nt_relVar = fabs(Nt_relVar);
	  }

	  // Possibility to apply pruning at this level already
	  if(fabs(eff_relVar)<pruning) eff_relVar = 0;
	  if(fabs(Nrt_relVar)<pruning) Nrt_relVar = 0;
	  if(fabs(Nt_relVar)<pruning) Nt_relVar = 0;

	  QCDscaleFile << std::string(6,' ') << "ATLAS_"+NP << ":" << std::endl; 
	  QCDscaleFile << std::string(8,' ') << "dEff: ["<<-eff_relVar<<", "<<eff_relVar<<", logn]" << std::endl;
	  QCDscaleFile << std::string(8,' ') << "dNrt: ["<<-Nrt_relVar<<", "<<Nrt_relVar<<", logn]" << std::endl;
	  QCDscaleFile << std::string(8,' ') << "dNt: ["<<-Nt_relVar<<", "<<Nt_relVar<<", logn]" << std::endl;

	}
      }
    }

    std::cout << "done." << std::endl;

    //===========================================================================================
    std::cout << "Writing PDF and AlphaS uncertainties..." << std::endl;
    PdfAndAlphaSFile << prodMode << ":" << std::endl;

    TH1D *h_Nr_nominal = h_Nrt_nominal.ProjectionX();// For now, integrate over truth bins to gain statistics
    double N_nom = h_Nt_nominal.Integral();

    for(int cat=0; cat<binning.getNRecoCat(); cat++){

      double Nr_nom = h_Nr_nominal->GetBinContent(cat+1);
      double eff_nom = Nr_nom==0 ? 0 : Nr_nom / N_nom;
      // Don't write uncertainties for bins with 0 efficiency
      if(eff_nom==0) continue;
      
      PdfAndAlphaSFile << std::string(2,' ') << binning.getRecoCatName(cat) << ":" << std::endl;

      // PDF uncertainties
      for(int i=0; i<30; i++){
	TString NP = Form("PDF4LHC_NLO_30_EV%i",i+1);

	TH2F h_Nrt_var = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP), merging, prodMode);
	TH1D *h_Nr_var = h_Nrt_var.ProjectionX(); // For now, integrate over truth bins to gain statistics
	TH1F h_Nt_var = binning.get1DHistWithMerging((TH1F*)f_in.Get("Nt_"+NP), merging, prodMode);
	double N_var = h_Nt_var.Integral();

	double Nr_var = h_Nr_var->GetBinContent(cat+1);
	double eff_var = Nr_var==0 ? 0 : Nr_var / N_var;

	double Nr_relVar = Nr_nom==0 ? 0 : (Nr_var-Nr_nom) / Nr_nom;
	if(fabs(Nr_relVar)<pruning) Nr_relVar = 0; 
	double N_relVar = Nr_nom==0 ? 0 : (N_var-N_nom) / N_nom;
	if(fabs(N_relVar)<pruning) N_relVar = 0;
	double eff_relVar = eff_nom==0 ? 0 : (eff_var-eff_nom) / eff_nom;
	if(fabs(eff_relVar)<pruning) eff_relVar = 0;

	PdfAndAlphaSFile << std::string(4,' ') << "ATLAS_"+NP << ":" << std::endl;
	PdfAndAlphaSFile << std::string(6,' ') << "dEff: ["<<-eff_relVar<<", "<<eff_relVar<<", logn]" << std::endl;
	PdfAndAlphaSFile << std::string(6,' ') << "dNrt: ["<<-Nr_relVar<<", "<<Nr_relVar<<", logn]" << std::endl;
	PdfAndAlphaSFile << std::string(6,' ') << "dNt: ["<<-N_relVar<<", "<<N_relVar<<", logn]" << std::endl;

      }

      // alphaS uncertainties
      TString NP = "PDF4LHC_NLO_30_alphaS";
      TH2F h_Nrt_up = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP+"_up"), merging, prodMode);
      TH1D *h_Nr_up = h_Nrt_up.ProjectionX(); // For now, integrate over truth bins to gain statistics
      TH1F h_Nt_up = binning.get1DHistWithMerging((TH1F*)f_in.Get("Nt_"+NP+"_up"), merging, prodMode);
      double N_up = h_Nt_up.Integral();
      TH2F h_Nrt_down = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP+"_down"), merging, prodMode);
      TH1D *h_Nr_down = h_Nrt_down.ProjectionX(); // For now, integrate over truth bins to gain statistics
      TH1F h_Nt_down = binning.get1DHistWithMerging((TH1F*)f_in.Get("Nt_"+NP+"_down"), merging, prodMode);
      double N_down = h_Nt_down.Integral();

      double Nr_up = h_Nr_up->GetBinContent(cat+1);
      double Nr_relUp = Nr_nom==0 ? 0 : (Nr_up-Nr_nom) / Nr_nom;
      double Nr_down = h_Nr_down->GetBinContent(cat+1);
      double Nr_relDown = Nr_nom==0 ? 0 : (Nr_down-Nr_nom) / Nr_nom;
      if(fabs(Nr_relUp)<pruning && fabs(Nr_relDown)<pruning) Nr_relUp = Nr_relDown = 0;

      double N_relUp = Nr_nom==0 ? 0 : (N_up-N_nom) / N_nom;
      double N_relDown = Nr_nom==0 ? 0 : (N_down-N_nom) / N_nom;
      if(fabs(N_relUp)<pruning && fabs(N_relDown)<pruning) N_relUp = N_relDown = 0;

      double eff_up = Nr_up==0 ? 0 : Nr_up / N_up;
      double eff_relUp = eff_nom==0 ? 0 : (eff_up-eff_nom) / eff_nom;
      double eff_down = Nr_down==0 ? 0 : Nr_down / N_down;
      double eff_relDown = eff_nom==0 ? 0 : (eff_down-eff_nom) / eff_nom;
      if(fabs(eff_relUp)<pruning && fabs(eff_relDown)<pruning) eff_relUp = eff_relDown = 0;

      PdfAndAlphaSFile << std::string(4,' ') << "ATLAS_"+NP << ":" << std::endl;
      PdfAndAlphaSFile << std::string(6,' ') << "dEff: ["<<eff_relDown<<", "<<eff_relUp<<", logn]" << std::endl;
      PdfAndAlphaSFile << std::string(6,' ') << "dNrt: ["<<Nr_relDown<<", "<<Nr_relUp<<", logn]" << std::endl;
      PdfAndAlphaSFile << std::string(6,' ') << "dNt: ["<<N_relDown<<", "<<N_relUp<<", logn]" << std::endl;
 
    }

    std::cout << "done." << std::endl;

    f_in.Close();

  }

  effFile.close();
  QCDscaleFile.close();
  PdfAndAlphaSFile.close();

  return 0;
}
