#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TMath.h"
#include "TGaxis.h"

#include "runExperimentalSys/AnalysisBinning.h"

int main(int argc, char *argv[]){

  AnalysisBinning binning;

  // Setup input tree
  //---------------------------------------------------------------------------------------

  TString inputFile = TString(argv[1]);
  TFile f_in(inputFile);
  TTree *t = (TTree*)f_in.Get("output");

  Float_t pTH; t->SetBranchAddress("pTH", &pTH);
  Float_t pTH_truth; t->SetBranchAddress("pTH_truth", &pTH_truth);
  Float_t pTHjj; t->SetBranchAddress("pTHjj", &pTH);
  Float_t pTHjj_truth; t->SetBranchAddress("pTHjj_truth", &pTHjj_truth);
  Int_t Nj; t->SetBranchAddress("N_j_30", &Nj);
  Int_t Nj_truth; t->SetBranchAddress("N_j_30_truth", &Nj_truth);
  Int_t cat; t->SetBranchAddress("recoCat", &cat);
  Int_t bin; t->SetBranchAddress("truthBin", &bin);
  std::vector<TString> QCDNPs = {"nom","QCD_mu","QCD_res","QCD_mig01","QCD_mig12","QCD_vbf2j","QCD_vbf3j","QCD_pTH60","QCD_pTH120","QCD_qm_t"};
  const int NNP = QCDNPs.size();
  Float_t weights[NNP];
  for(int i=0; i<QCDNPs.size(); i++) t->SetBranchAddress(Form("w_%s",QCDNPs.at(i).Data()), &weights[i]);

  // Define histograms
  //---------------------------------------------------------------------------------------

  std::map<TString,TH1F*> h_pTH;
  std::map<TString,TH1F*> h_pTHjj;
  std::map<TString,TH1F*> h_Nj;

  for(TString sys : QCDNPs){
    for(int i=0; i<30; i++){
      for(int j=0; j<31; j++){
	TString name = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),sys.Data());
	h_pTH[name] = new TH1F(name+"pTH","",40,0,300);
	h_pTHjj[name] = new TH1F(name+"pTHjj","",40,0,300);
	h_Nj[name] = new TH1F(name+"Nj","",7,0,7);
      }
      TString name = Form("%s_%s",binning.getTruthBinName(i).Data(),sys.Data());
      h_pTH[name] = new TH1F(name+"pTH","",40,0,300);
      h_pTHjj[name] = new TH1F(name+"pTHjj","",40,0,300);
      h_Nj[name] = new TH1F(name+"Nj","",7,0,7);
    }
  }

  // Fill histograms
  //---------------------------------------------------------------------------------------
  Long_t entries = t->GetEntries();
  for(int i=0; i<entries; i++){
    t->GetEntry(i);
    if(i%(int(entries/100)) == 0) std::cout<<"\rEvents processed: "<<(int)(i*100/(1.*entries))<<"%"<<std::flush;
    for(int s=0; s<QCDNPs.size(); s++){
      if(bin<0 || bin>30) continue;
      TString name = Form("%s_%s",binning.getTruthBinName(bin).Data(),QCDNPs.at(s).Data());
      h_pTH[name]->Fill(pTH_truth/1000.,weights[s]);
      h_pTHjj[name]->Fill(pTHjj_truth/1000.,weights[s]);
      h_Nj[name]->Fill(Nj_truth,weights[s]);
      if(cat<0 || cat>30) continue;
      name = Form("%s_%s_%s",binning.getTruthBinName(bin).Data(),binning.getRecoCatName(cat).Data(),QCDNPs.at(s).Data());
      // h_pTH[name]->Fill(pTH/1000.,weights[s]);
      // h_pTHjj[name]->Fill(pTHjj/1000.,weights[s]);
      // h_Nj[name]->Fill(Nj,weights[s]);
      h_pTH[name]->Fill(pTH_truth/1000.,weights[s]);
      h_pTHjj[name]->Fill(pTHjj_truth/1000.,weights[s]);
      h_Nj[name]->Fill(Nj_truth,weights[s]);
    }
  }


  // Make pull histograms
  //---------------------------------------------------------------------------------------
  for(TString sys : QCDNPs){
    for(int i=0; i<30; i++){
      TString nameNtnom = Form("%s_nom",binning.getTruthBinName(i).Data());
      for(int j=0; j<31; j++){
	TString nameNrtnom = Form("%s_%s_nom",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data());
	TString name = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),sys.Data());
	h_pTH["pull_"+name] = (TH1F*)h_pTH[name]->Clone("pull_"+name);
	h_pTH["pull_"+name]->Add(h_pTH[nameNrtnom],-1);
	h_pTH["pull_"+name]->Divide(h_pTH[nameNrtnom]);
	h_pTHjj["pull_"+name] = (TH1F*)h_pTHjj[name]->Clone("pull_"+name);
	h_pTHjj["pull_"+name]->Add(h_pTHjj[nameNrtnom],-1);
	h_pTHjj["pull_"+name]->Divide(h_pTHjj[nameNrtnom]);
	h_Nj["pull_"+name] = (TH1F*)h_Nj[name]->Clone("pull_"+name);
	h_Nj["pull_"+name]->Add(h_Nj[nameNrtnom],-1);
	h_Nj["pull_"+name]->Divide(h_Nj[nameNrtnom]);
      }
      TString name = Form("%s_%s",binning.getTruthBinName(i).Data(),sys.Data());
      h_pTH["pull_"+name] = (TH1F*)h_pTH[name]->Clone("pull_"+name);
      h_pTH["pull_"+name]->Add(h_pTH[nameNtnom],-1);
      h_pTH["pull_"+name]->Divide(h_pTH[nameNtnom]);
      h_pTHjj["pull_"+name] = (TH1F*)h_pTHjj[name]->Clone("pull_"+name);
      h_pTHjj["pull_"+name]->Add(h_pTHjj[nameNtnom],-1);
      h_pTHjj["pull_"+name]->Divide(h_pTHjj[nameNtnom]);
      h_Nj["pull_"+name] = (TH1F*)h_Nj[name]->Clone("pull_"+name);
      h_Nj["pull_"+name]->Add(h_Nj[nameNtnom],-1);
      h_Nj["pull_"+name]->Divide(h_Nj[nameNtnom]);
    }
  }

  std::vector<std::map<TString,TH1F*>> maps = {h_pTH, h_pTHjj, h_Nj};
  std::vector<TString> labels = {"p_{T}^{H} / GeV", "p_{T}^{Hjj} / GeV", "N_{j}"};


  // Colors and legend
  //---------------------------------------------------------------------------------------
  std::vector<Color_t> colors = {kBlack, kGray, kYellow, kCyan, kGreen+1, kRed, kBlue, kMagenta, kOrange, kGreen};

  for(int m=0; m<maps.size(); m++){
    for(int i=0; i<30; i++){
      for(int j=0; j<31; j++){
	for(int s=0; s<QCDNPs.size(); s++){
	  TString nameNrt = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),QCDNPs.at(s).Data());
	  maps[m][nameNrt]->SetLineColor(colors.at(s));
	  maps[m][nameNrt]->SetXTitle(labels.at(m));
	  maps[m]["pull_"+nameNrt]->SetLineColor(colors.at(s));
	  maps[m]["pull_"+nameNrt]->SetXTitle(labels.at(m));
	  TString nameNt = Form("%s_%s",binning.getTruthBinName(i).Data(),QCDNPs.at(s).Data());
	  maps[m][nameNt]->SetLineColor(colors.at(s));
	  maps[m][nameNt]->SetLineStyle(kDashed);
	  maps[m]["pull_"+nameNt]->SetLineColor(colors.at(s));
	  maps[m]["pull_"+nameNt]->SetLineStyle(kDashed);
	}
      }
    }
  }
  TLegend leg(0.6,0.6,0.85,0.85);
  leg.SetBorderSize(0);
  for(TString s : QCDNPs) leg.AddEntry(h_pTH[Form("%s_%s_%s",binning.getTruthBinName(0).Data(),binning.getRecoCatName(0).Data(),s.Data())],s,"l");


  // Draw plots
  //---------------------------------------------------------------------------------------
  TCanvas *c = new TCanvas();
  gStyle->SetOptStat(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  TGaxis::SetMaxDigits(7);
  c->Print("plot_weightedDist.pdf[");

  TPad *pad1 = new TPad("pad1","",0.,0.3,1.,1.);
  pad1->SetBottomMargin(0.02);
  pad1->SetLeftMargin(0.13);
  pad1->SetRightMargin(0.02);
  pad1->Draw();

  TPad *pad2 = new TPad("pad2","",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.04);
  pad2->SetBottomMargin(0.3);
  pad2->SetLeftMargin(0.13);
  pad2->SetRightMargin(0.02);
  pad2->Draw();

  for(std::map<TString,TH1F*> m : maps){


    for(int i=0; i<11; i++){
      double Ntnom = m[Form("%s_nom",binning.getTruthBinName(i).Data())]->Integral();

      for(int j=0; j<31; j++){
	double Nrtnom = m[Form("%s_%s_nom",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data())]->Integral();

	if(Nrtnom/Ntnom<0.1) continue;

	double max = 0;
	double maxPull = 0;
	double minPull = 0;
	for(int s=0; s<QCDNPs.size(); s++){
	  TString nameNrt = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),QCDNPs.at(s).Data());
	  TString nameNt = Form("%s_%s",binning.getTruthBinName(i).Data(),QCDNPs.at(s).Data());
	  if(Nrtnom>0) m[nameNrt]->Scale(1./Nrtnom);
	  if(Ntnom>0) m[nameNt]->Scale(1./Ntnom);
	  max = m[nameNrt]->GetMaximum()>max ? m[nameNrt]->GetMaximum() : max;
	  max = m[nameNt]->GetMaximum()>max ? m[nameNt]->GetMaximum() : max;
	  maxPull = m["pull_"+nameNrt]->GetMaximum()>maxPull ? m["pull_"+nameNrt]->GetMaximum() : maxPull;
	  maxPull = m["pull_"+nameNt]->GetMaximum()>maxPull ? m["pull_"+nameNt]->GetMaximum() : maxPull;
	  minPull = m["pull_"+nameNrt]->GetMinimum()<minPull ? m["pull_"+nameNrt]->GetMinimum() : minPull;
	  minPull = m["pull_"+nameNt]->GetMinimum()<minPull ? m["pull_"+nameNt]->GetMinimum() : minPull;
	}

	bool first=true;
	for(int s=0; s<QCDNPs.size(); s++){
	  TString option = "hist";
	  if(!first) option += " same";
	  first=false;
	  TString nameNrt = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),QCDNPs.at(s).Data());
	  TString nameNt = Form("%s_%s",binning.getTruthBinName(i).Data(),QCDNPs.at(s).Data());
	  m[nameNrt]->SetMaximum(max*1.1);
	  m[nameNrt]->SetTitle(Form("%s, %s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data()));
	  m[nameNrt]->GetXaxis()->SetLabelSize(0);
	  m[nameNrt]->GetXaxis()->SetTitleSize(0);

	  pad1->cd();
	  m[nameNrt]->Draw(option);
	  m[nameNt]->Draw("hist same");

	  pad2->cd();
	  m["pull_"+nameNrt]->SetTitle("");
	  m["pull_"+nameNrt]->GetXaxis()->SetLabelSize(0.06);
	  m["pull_"+nameNrt]->GetXaxis()->SetTitleSize(0.06);
	  m["pull_"+nameNrt]->SetMinimum(minPull*1.1);
	  m["pull_"+nameNrt]->SetMaximum(maxPull*1.1);
	  m["pull_"+nameNrt]->Draw(option);
	  m["pull_"+nameNt]->Draw("hist same");
	}

	pad1->cd();
	m[Form("%s_%s_nom",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data())]->Draw("hist same");
	m[Form("%s_nom",binning.getTruthBinName(i).Data())]->Draw("hist same");
	leg.Draw("same");

	c->Print("plot_weightedDist.pdf");
      }
    }
  }

  c->Print("plot_weightedDist.pdf]");
  delete pad1; delete pad2;
  delete c;

  // Clean up memory
  //----------------------------------------------------------------------------------
  for(TString sys : QCDNPs){
    for(int i=0; i<31; i++){
      for(int j=0; j<31; j++){
	TString name = Form("%s_%s_%s",binning.getTruthBinName(i).Data(),binning.getRecoCatName(j).Data(),sys.Data());
	delete h_pTH[name];
	delete h_pTHjj[name];
	delete h_Nj[name];
      }
      TString name = Form("%s_%s",binning.getTruthBinName(i).Data(),sys.Data());
      delete h_pTH[name];
      delete h_pTHjj[name];
      delete h_Nj[name];
    }
  }

  delete t;
  f_in.Close();
  return 0;
}
