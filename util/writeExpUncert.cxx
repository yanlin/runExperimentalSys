#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TKey.h"

#include "runExperimentalSys/AnalysisBinning.h"

int main(int argc, char *argv[]){

  int opt; 
  extern char *optarg;

  // NOTE: merging could become important if we decide to use a higher granularity!

  TString inputFile="";
  TString sysType="";
  TString merging="stage1";
  TString mode="ALL";
  double pruning=0.0005;

  while((opt = getopt(argc, argv, "i:s:p:m:x:h")) != -1){
    switch(opt){
    case 'i':
      inputFile = TString(optarg);
      break;
    case 's':
      sysType = TString(optarg);
      break;
    case 'p':
      mode = TString(optarg);
      break;
    case 'm':
      merging = TString(optarg);
      break;
    case 'x':
      pruning = atof(optarg);
      break;
    case 'h':
      std::cout << "i: name of input file; put ? as place holder for production mode" << std::endl;
      std::cout << "s: systematics category" << std::endl;
      std::cout << "m: merging (default: stage1, other options: strong, production)" << std::endl;
      std::cout << "p: only a single production mode" << std::endl;
      std::cout << "x: pruning (default: 0.05%)" << std::endl;
      return 0;
    }
  }

  if(inputFile.Length()==0 || !inputFile.EndsWith(".root")){
    std::cout << "Please provide an input file in .root format (containing histograms)" << std::endl;
    return -1;
  }

  AnalysisBinning binning;
  std::vector<TString> prodModes;
  if(mode=="ALL") prodModes =  binning.getProductionModes();
  else prodModes = {mode};

  std::ofstream sysFile;
  //sysFile.open("FileLists/yamlFiles/"+sysType+"_"+merging+".yaml");
  sysFile.open("FileLists/yamlFiles/"+mode+"_"+sysType+".yaml");

  for(TString prod : prodModes){
    std::cout << prod << std::endl;
    sysFile << prod << ":" << std::endl;

    TString inFile = inputFile.Copy();
    inFile.ReplaceAll("?",prod);
    TFile f_in(inFile);
    TH2F h_Nrt_nom = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_nominal"), merging, prod);
    //TH2F *h_Nrt_nom = (TH2F*) f_in.Get("Nrt_nominal");
    TH1D *h_Nr_nom = h_Nrt_nom.ProjectionX();
    std::cout << "Found nominal histograms" << std::endl;
    //std::cout << "contents: "<<h_Nr_nom->GetBinContent(3)<<", "<<h_Nr_nom->GetBinContent(6)<<", "<<h_Nr_nom->Integral() << std::endl;
    std::vector<TString> NPlist;
    TIter next(f_in.GetListOfKeys());
    TKey *key;
    while((key=(TKey*)next())){
      TH2F *h_tmp = (TH2F*)key->ReadObj();   
      TString h_name = h_tmp->GetName();
      delete h_tmp;
      if(h_name.Contains("Nrt_") && h_name!="Nrt_nominal"){
	h_name.ReplaceAll("Nrt_","");
	NPlist.push_back(h_name);
      }
    }
    delete key;

    for(int cat=0; cat<binning.getNRecoCat(); cat++){
      std::cout<<"cat: "<<cat<<std::endl;
      sysFile << std::string(2,' ') << binning.getRecoCatName(cat) << ":" << std::endl;
      double nom = h_Nr_nom->GetBinContent(cat+1);
      for(TString NP : NPlist){
        if(cat == 0) std::cout<<"NP: "<<NP<<std::endl;
        //if(!NP.Contains("PRW")) continue;
	if(NP.Contains("up",TString::kIgnoreCase)) continue;
	TH2F h_Nrt_down = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP), merging, prod);
	TH1D *h_Nr_down = h_Nrt_down.ProjectionX();
	double relVar_down = nom==0 ? 0 :(h_Nr_down->GetBinContent(cat+1)-nom)/nom;
	NP.ReplaceAll("down","up");
	NP.ReplaceAll("Down","Up");
	TH2F h_Nrt_up = binning.get2DHistWithMerging((TH2F*)f_in.Get("Nrt_"+NP), merging, prod);
	TH1D *h_Nr_up = h_Nrt_up.ProjectionX();
	double relVar_up = nom==0 ? 0 : (h_Nr_up->GetBinContent(cat+1)-nom)/nom;
        //std::cout<<"pruning: "<<pruning<<std::endl;
	if(fabs(relVar_up)<pruning) relVar_up = 0;
	if(fabs(relVar_down)<pruning) relVar_down = 0;
        if(fabs(relVar_up)/fabs(relVar_down) > 5 || fabs(relVar_down)/fabs(relVar_up) > 5){
           relVar_up = 0;
           relVar_down = 0;
        }
	NP.ReplaceAll("__1up","");
	NP.ReplaceAll("Up","");
	sysFile << std::string(4,' ') << "ATLAS_"+NP << ": ["<<relVar_down<<", "<<relVar_up<<", asym]" << std::endl;
	if(h_Nr_down!=h_Nr_up) delete h_Nr_down; 
	delete h_Nr_up;
      }
    }

  }

  sysFile.close();
  std::cout << "Wrote uncertainties to yaml file" << std::endl;

  return 0;
}

