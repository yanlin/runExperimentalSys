##Modified based on the code from Saskia:https://gitlab.cern.ch/lbnl/ttHyy/theory-systs

##First setup analysis base release and clone the HGamCore

mkdir build source run

cd source

setupATLAS

asetup AnalysisBase,21.2.56,here

git clone --recursive https://:@gitlab.cern.ch:8443/atlas-hgam-sw/HGamCore.git

cd HGamCore

git checkout release/v1.8

git submodule update --init --recursive # Get proper dependencies

##Clone the runExperimentalSys package into source, and then compile

git clone https://:@gitlab.cern.ch:8443/yanlin/runExperimentalSys.git

cd ../build

cmake ../source

make -j4

##After compiling

source build/x86_64-centos7-gcc62-opt/setup.sh

##Process a systematic MxAOD file and then write systematic variation into yaml files

source runEffAndSystematics.sh ##output is saved into testdir/hist-yields.root

source writeExpUncert.sh ##output file is saved into directory FileLists/yamlFiles

##Submit condor jobs to process mutiple systematic MxAOD files

cd JobSubmission

python submit.py input.txt -b
